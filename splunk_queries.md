**Long Lasting Connections:**
```
index=<yourindexhere> sourcetype="stream:tcp" dest_ip!=<yourwhitelistshere> dest_port!=443 dest_port!=80
| eval minutes=(time_taken/60000000)
| table time_taken minutes src_ip dest_ip dest_port app
|sort -time_taken
| head 100
```

**Outbound SMTP Connections:**
```
| tstats summariesonly=t allow_old_summaries=t count(All_Traffic.src_ip) from datamodel=Network_Traffic where All_Traffic.transport=tcp (All_Traffic.dest_port=587 OR All_Traffic.app=smtp) All_Traffic.dest_ip!=<yourwhitelisthere> All_Traffic.src_ip=<yournetworkhere> groupby All_Traffic.dest_ip All_Traffic.src_ip All_Traffic.dest_port
| lookup dnslookup ip AS All_Traffic.dest_ip OUTPUT host AS domain 
| dedup domain
| table domain All_Traffic.dest_ip All_Traffic.src_ip All_Traffic.dest_port
```
**Dynamic DNS Domain Queried:**
```
| tstats count from datamodel=Network_Resolution by DNS.query 
| lookup dynamic_dns_providers_default.csv dynamic_dns_domains AS DNS.query 
| search isDynDNS_default=TRUE
```

